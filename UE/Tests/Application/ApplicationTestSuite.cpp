#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Application.hpp"
#include "Mocks/ILoggerMock.hpp"
#include "Messages/PhoneNumber.hpp"
#include <memory>

namespace ue
{
using namespace ::testing;

class ApplicationTestSuite : public Test
{
protected:
    const common::PhoneNumber PHONE_NUMBER{112};
    NiceMock<common::ILoggerMock> loggerMock;

    Expectation ignoreAllNonErrorLogs = EXPECT_CALL(loggerMock, log(Ne(ILogger::ERROR_LEVEL), _)).Times(AnyNumber());
    Application objectUnderTest{PHONE_NUMBER,
                                loggerMock};
};

struct ApplicationNotConnectedTestSuite : ApplicationTestSuite
{};
TEST_F(ApplicationNotConnectedTestSuite, shallComplainOnUnexpectedEvent)
{
}

}
